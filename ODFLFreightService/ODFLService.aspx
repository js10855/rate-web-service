﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ODFLService.aspx.cs" Inherits="ODFLFreightService.ODFLService" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Freight Calculator</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table border=0>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Destination ZIP Code" Font-Names="Arial" Font-Size="11pt"></asp:Label></td>
            <td>
            </td>
            <td align="left" style="font-size: 11px; font-family: Arial" >
                <asp:TextBox ID="txtDestinationZIP" runat="server"></asp:TextBox>
                (your ZIP Code)
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDestinationZIP"
                    ErrorMessage="Please enter a valid ZIP code" 
                    ValidationExpression="\d{5}(-\d{4})?|\w{6}">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDestinationZIP"
                    ErrorMessage="Destination ZIP is required">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="font-size: 11pt; font-family: Arial">
                Delivery Type</td>
            <td>
            </td>
            <td align="left">
                <asp:RadioButton ID="rbBusiness" runat="server" Checked="True" Font-Names="Arial"
                    Font-Size="11pt" GroupName="delivery" Text="Business" />
                <asp:RadioButton ID="rbResidential" runat="server" Font-Names="Arial" Font-Size="11pt"
                    GroupName="delivery" Text="Residential" /></td>
        </tr>
        <tr>
            <td style="font-size: 11pt; font-family: Arial">
                Quantity</td>
            <td>
            </td>
            <td align="left" style="font-size: 11px; font-family: Arial" >
                &nbsp;<asp:DropDownList ID="ddlQty" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList>
                (total floats being shipped)</td>
        </tr>
        <tr>
            <td style="font-size: 11pt; font-family: Arial">
            </td>
            <td>
            </td>
            <td align="right" style="font-size: 11px; font-family: Arial">
                &nbsp;<asp:LinkButton ID="cmdGetRate" runat="server" Font-Names="Arial" Font-Size="11pt" OnClick="cmdGetRate_Click">Get Rate</asp:LinkButton>
                <asp:LinkButton ID="cmdCancel" runat="server" Font-Names="Arial" Font-Size="11pt">Cancel</asp:LinkButton></td>
        </tr>
        <tr>
            <td style="font-size: 11pt; font-family: Arial">
            </td>
            <td>
            </td>
            <td align="right" style="font-size: 11px; font-family: Arial">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" style="font-size: 11pt; font-family: Arial; height: 21px">
                Home business does not count as business, must be a commercial address</td>
        </tr>
        <tr>
            <td align="center" colspan="3" style="font-size: 11pt; font-family: Arial; height: 21px">
                For shipments over 10 floats please call sales at 1-888-PWC-Lift toll free for special
                pricing</td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td align="right">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt"
            ForeColor="Red"></asp:Label><br />
    </div>
        <asp:GridView ID="gvRates" runat="server" BackColor="White" BorderColor="#3366CC"
            BorderStyle="None" BorderWidth="1px" CellPadding="4" 
        Font-Names="Arial" Font-Size="11pt" AutoGenerateColumns="False">
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <Columns>
            <asp:BoundField DataField="Carriername" HeaderText="Carrier name" />
                <asp:BoundField DataField="FinalBillAmountStr" HeaderText="Final Freight Bill Amount" />
            </Columns>
        </asp:GridView>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt" />
    </form>
</body>
</html>

