﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Net;
using System.IO;
using ODFLFreightService.RateServiceReference;
using System.Configuration;
using System.Data;

namespace ODFLFreightService
{
    public partial class ODFLService : System.Web.UI.Page
    {       

        private decimal GetLTLRate(string originPostalCode, string destinationPostalCode, int weight,long odflCustomerAccount, ref bool success, ref string error)
        {
            decimal result = 0;
            RateDelegateClient service = new RateDelegateClient();
            myRateRequest request = new myRateRequest();   
            request.originPostalCode = originPostalCode;
            request.originCountry = "usa";
            request.destinationPostalCode = destinationPostalCode;
            request.destinationCountry = "usa";
            freight freight = new freight();
            freight.ratedClass =Convert.ToInt32(ConfigurationManager.AppSettings.Get("ratedClass")); ;
            freight.ratedClassSpecified = true;
            freight.weight = weight;
            freight[] items = new freight[1];
            items[0] = freight;
            request.freightItems = items;
            string[] strAccessorial = { "RDC" };
            if (rbResidential.Checked) request.accessorials = strAccessorial;
            //request.tariff = 559;
            //request.tariffSpecified = true;
            // Uncomment below and fill in to retrieve customized pricing
            request.odfl4MeUser = ConfigurationManager.AppSettings.Get("odfl4MeUser");
            request.odfl4MePassword = ConfigurationManager.AppSettings.Get("odfl4MePassword");
            request.odflCustomerAccount = odflCustomerAccount; 
            //request.odflCustomerAccountSpecified = true;
            
            myLTLRateResponse response = service.getLTLRateEstimate(request);
            
            if (response.success)
            {
                success = true;
                //result = response.rateEstimate.netFreightCharge;
                result = response.rateEstimate.grossFreightCharge -response.rateEstimate.discountAmount + response.rateEstimate.fuelSurcharge;
                if (rbResidential.Checked) result += response.rateEstimate.accessorialCharges[0].amount;
            }
            else
            {
                success = false;
                string err="";
                foreach (string s in response.errorMessages)
                {
                    err += s;
                }
                error = err;
            }
            return result;
        }       

        protected void cmdGetRate_Click(object sender, EventArgs e)
        {            
            string originPostalCode1 = ConfigurationManager.AppSettings.Get("originPostalCode1");
            string originPostalCode2 = ConfigurationManager.AppSettings.Get("originPostalCode2");
            long odflCustomerAccount1= Convert.ToInt64(ConfigurationManager.AppSettings.Get("odflCustomerAccount1"));
            long odflCustomerAccount2 = Convert.ToInt64(ConfigurationManager.AppSettings.Get("odflCustomerAccount2"));

            string destinationPostalCode = txtDestinationZIP.Text;

            int totalWeight =Convert.ToInt32(ConfigurationManager.AppSettings.Get("itemweight"));
            int extraWeight =Convert.ToInt32(ConfigurationManager.AppSettings.Get("PalletWeight"));
            int weight = totalWeight * Convert.ToInt32(ddlQty.SelectedValue) + extraWeight;

            bool success=false;
            string error = "";
            decimal rate1 = 0;
            decimal rate2 = 0;
            rate1= GetLTLRate(originPostalCode1, destinationPostalCode, weight,odflCustomerAccount1, ref success,ref error);

            if (success)
            {
                rate2= GetLTLRate(originPostalCode2, destinationPostalCode, weight, odflCustomerAccount2, ref success, ref error);
                if (success)
                {
                    decimal finalRate = Math.Min(rate1, rate2);
                    Decimal Upcharge = Convert.ToDecimal(ConfigurationManager.AppSettings.Get("Upcharge"));
                    //finalRate = finalRate + Upcharge;
                    BindGrid(finalRate + Upcharge,finalRate);
                }
                else
                {
                    lblMessage.Text = error;
                }
            }
            else
            {
                lblMessage.Text = error;
            }
        }

        private void BindGrid(decimal finalbillamount,decimal netfreightcharges)
        {
            //List rates (bind to grid)
            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add("CarrierCodeId");
            dt.Columns.Add("CarrierName");
            dt.Columns.Add("FinalBillAmount");
            dt.Columns.Add("FinalBillAmountStr");
            dt.Columns["FinalBillAmount"].DataType = System.Type.GetType("System.Decimal");
            dt.Columns.Add("CHARGE");
            dt.Columns.Add("CHARGEStr");
            dt.Columns.Add("RESIDENTIALDELIVERY");
            dt.Columns.Add("RESIDENTIALDELIVERYStr");
            dt.Columns.Add("FUELSURCHARGE");
            dt.Columns.Add("FUELSURCHARGEStr");
            dt.Columns.Add("GUARANTEEDCHARGE");
            dt.Columns.Add("GUARANTEEDCHARGEStr");

            dr = dt.NewRow();
            dr["CarrierCodeId"] = ConfigurationManager.AppSettings.Get("abfsCarrierCode");
            dr["CarrierName"] = ConfigurationManager.AppSettings.Get("abfsDisplayName");
            //dr.Item("EstimatedServiceDays") = f.ServiceDays + LoadTime

            //If rbResidential.Checked Then
            //    'finalbillamount = CHARGE + Upcharge
            //    finalbillamount = f.Result.netfreightcharges + Upcharge
            //ElseIf rbBusiness.Checked Then
            //    'finalbillamount = CHARGE + Upcharge
            //    finalbillamount = f.Result.netfreightcharges + Upcharge
            //End If


            dr["FinalBillAmount"] = Convert.ToDecimal(finalbillamount);
            dr["FinalBillAmountStr"] = finalbillamount.ToString("C");


            dr["CHARGEStr"] = netfreightcharges.ToString();
            //dr["RESIDENTIALDELIVERYStr"] = f.Result.residentialdelivery.ToString;
            //dr["FUELSURCHARGEStr"] = f.Result.fuelsurcharge.ToString;
            //dr["GUARANTEEDCHARGEStr"] = f.Result.guaranteedcharge.ToString;

            dt.Rows.Add(dr);

            DataView dv = dt.DefaultView;
            dv.Sort = "finalbillamount asc";
            gvRates.DataSource = dv;
            gvRates.DataBind();
        }
    }

 }